FROM nginx:stable-alpine

WORKDIR /etc/nginx/conf.d

COPY nginx/nginx.conf .

RUN mv nginx.conf default.conf

EXPOSE 80

WORKDIR /var/www/html

COPY src .
