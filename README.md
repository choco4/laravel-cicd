# Blueprint Laravel microservice project

### App containers [Laravel, MySQL, Nginx]
### Utility containers [Composer, Artisan]

---

### Create laravel project
```
docker-compose run --rm composer create-project --prefer-dist laravel/laravel .
```

### Install laravel dependency (vendor)
```
docker-compose run --rm composer install
```

### Start project
```
docker-compose up -d --build server
```

### Example artisan command
```
docker-compose run --rm artisan migrate
```